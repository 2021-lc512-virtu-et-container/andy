Q1 :
```mermaid
sequenceDiagram
    docker->>dockerd: Contact
    dockerd->>cache locale: Looking if image is already pull locally
    cache locale->>dockerd: Send
    dockerd->>docker: Send
    dockerd->>hub docker: Request image
    hub docker->>dockerd: Pull image
    dockerd->>cache locale: Store cache
    dockerd->>docker: Output result
```
Q2 :
Les TAG correspondent aux versions des fichiers

